//
//  login65appsTests.swift
//  login65appsTests
//
//  Created by Aleksandr Konyshev on 08.02.2020.
//  Copyright © 2020 Aleksandr Konyshev. All rights reserved.
//

import XCTest
@testable import login65apps

class login65appsTests: XCTestCase {
    
    //MARK: length
    func testInvalidMinLogin(){
        let user = User(login: "ew")
        
        XCTAssertFalse(user.validLengthLogin())
    }
    
    func testValidMinLogin(){
        let user = User(login: "ewa")
        
        XCTAssertTrue(user.validLengthLogin())
    }
    
    func testInvalidMaxLogin(){
        let user = User(login: "bghfdrtyhgfdrtyhgfdcvbgtrfdsclkjll")
        
        XCTAssertFalse(user.validLengthLogin())
    }
    
    func testValidMaxLogin(){
        let user = User(login: "eyfhdjjhgvbhhghjjkkkkkkkkikdhhff")
        
        XCTAssertTrue(user.validLengthLogin())
    }
    
    //MARK: charsets
    func testValidCarsetsLogin(){
        let user = User(login: "alddl-434353.vdsvds")
        
        XCTAssertTrue(user.validCharsets())
    }
    
    func testInvalidCarsetsLogin1(){
        let user = User(login: "kkdjsk\\fldfdsdf")
        
        XCTAssertFalse(user.validCharsets())
    }
    
    func testInvalidCarsetsLogin2(){
        let user = User(login: "kkdjskfl{dfd}sdf")
        
        XCTAssertFalse(user.validCharsets())
    }
    
    func testInvalidCarsetsLogin3(){
        let user = User(login: "kkdjsk fldfdsdf")
        
        XCTAssertFalse(user.validCharsets())
    }
    
    func testInvalidCarsetsLogin4(){
        let user = User(login: "kkdjsk%dfdsdf")
        
        XCTAssertFalse(user.validCharsets())
    }
    
    func testInvalidCarsetsLogin5(){
        let user = User(login: "kkdjskf$dsdf")
        
        XCTAssertFalse(user.validCharsets())
    }
    
    //MARK: first charset
    func testValidFirstCharsetLogin(){
        let user = User(login: "eyfhdjjhgvbhhghjjkkkkkkkkikdhhff")
        
        XCTAssertTrue(user.validFirstCharset())
    }
    
    func testInvalidFirstCharsetLogin(){
        let user = User(login: ".yfhdjjhgvbhhghjjkkkkkkkkikdhhff")
        
        XCTAssertFalse(user.validFirstCharset())
    }
    
    func testInvalidFirstCharsetLogin2(){
        let user = User(login: "-yfhdjjhgvbhhghjjkkkkkkkkikdhhff")
        
        XCTAssertFalse(user.validFirstCharset())
    }
    
    func testInvalidFirstCharsetLogin3(){
        let user = User(login: "3422hdjjhgvbhhghjjkkkkkkkkikdhhff")
        
        XCTAssertFalse(user.validFirstCharset())
    }
    
    //MARK: valid email
    func testValidEmailLogin(){
        let user = User(login: "al@mail.ru")
        
        XCTAssertTrue(user.validEmail())
    }
    
    func testInvalidEmailLogin1(){
        let user = User(login: "вы@mail.ru")
        
        XCTAssertFalse(user.validEmail())
    }
    
    func testInvalidEmailLogin2(){
        let user = User(login: "fss@$5fdsf.ru")
        
        XCTAssertFalse(user.validEmail())
    }
    
    func testInvalidEmailLogin3(){
        let user = User(login: "fss@fru")
        
        XCTAssertFalse(user.validEmail())
    }
    
    func testInvalidEmailLogin4(){
        let user = User(login: "fss@авав.ru")
        
        XCTAssertFalse(user.validEmail())
    }
    
    func testInvalidEmailLogin5(){
        let user = User(login: "fss@.ru")
        
        XCTAssertFalse(user.validEmail())
    }
}
