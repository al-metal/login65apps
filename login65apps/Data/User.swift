//
//  User.swift
//  login65apps
//
//  Created by Aleksandr Konyshev on 09.02.2020.
//  Copyright © 2020 Aleksandr Konyshev. All rights reserved.
//

import Foundation

class User{
    let login: String
    var errors: [String] = []
    
    enum Regex: String{
        case length = ".{3,30}"
        case charsets = "[a-zA-Z0-9-.]"
        case firstCharset = "[a-zA-Z]"
        case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    }
    
    init(login: String) {
        self.login = login
    }
    
    func validLengthLogin() -> Bool{
        return regular(Regex.length.rawValue, login.count, 1)
    }
    
    func validCharsets() -> Bool{
        return regular(Regex.charsets.rawValue, login.count, login.count)
    }
    
    func validFirstCharset() -> Bool{
        return regular(Regex.firstCharset.rawValue, 1, 1)
    }
    
    fileprivate func regular(_ regex: String, _ lengthLogin: Int, _ Comparison: Int) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let result = regex.matches(in: login, range: NSRange(location: 0, length: lengthLogin))// (login.startIndex..., in: login))
            
            if result.count != Comparison { return false }
        }catch let error{
            print(error)
            return false
        }
        return true
    }
    
    func validEmail() -> Bool{
        return NSPredicate(format: "SELF MATCHES %@", Regex.email.rawValue).evaluate(with: login)
    }
    
    func validLogin() -> [String]{
        errors = []
        
        if login.contains("@"){
            if !validEmail(){ errors.append("Введен не корректный email") }
        } else{
            
            if !validLengthLogin(){ errors.append("Длинна логина должна быть от 3х до 32х символов") }
            if !validCharsets(){ errors.append("Логин может содержать только латинские буквы и символы . -") }
            if !validFirstCharset(){ errors.append("Логин может начинаться только латинские буквы") }
            
        }
        return errors
    }
}
