//
//  ViewController.swift
//  login65apps
//
//  Created by Aleksandr Konyshev on 08.02.2020.
//  Copyright © 2020 Aleksandr Konyshev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var field: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func signIn(_ sender: UIButton) {
        view.endEditing(true)
        errorLabel.text = ""
        
        guard let login = field.text else { return }
        
        let user = User(login: login)
        
        let errors: [String] = user.validLogin()
        
        if errors.count > 0 {
            for err in errors{
                errorLabel.text?.append("\(err)\r\n")
            }
        }
    }
}
